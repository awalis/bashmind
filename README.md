# BASH MIND

>>>
Ce fichier est bilingue (Anglais - Français)

This file is bilingual (English - French)
>>>

[[_TOC_]]

## English

BASH MIND is a command line version of the game [Mastermind or Code Breaker](http://codebreaker.creativitygames.net/) written in [Bash](https://www.gnu.org/software/bash/). You use digits instead of coloured marbles. 

### Basic Usage

1. Get a copy of the `bashmind.sh` script (download or clone it),

2. Open your terminal emulator (obviously),

3. Grant the script file *execute* permission,

		$ chmod +x bashmind.sh

4. Run the script and Crack the Code!

		$ ./bashmind.sh


### Customization

Game parameters, i.e. **(1) Secret code length**, **(2) number of Items** to choose from and **(3) how many Trials** you get to break the code, can be changed by adding parameters to the command line at launch time, in the listed order.

	./bashmind.sh 4 6 8

This command starts the game with the following parameters:

1. Secret Code Length: **4** digits, say "3 1 0 5"
2. Number of Items to choose from: **6**, that is the six digits "0 1 2 3 4 and 5"
3. Maximum Number of Trials: **8**.


### Requirement

A terminal emulator with Bash as a shell.

"**Bash** is the default shell on most Linux distributions and Apple's macOS (formerly OS X). Recently, a version has also been made available for Windows 10." -- [Ryans Tutorials](https://ryanstutorials.net/bash-scripting-tutorial/)


### Idea

This script started as two of the activities in the Bash Scripting Tutorial by Ryan Chadwick. After finishing the tutorial and having fun playing it with a few friends, I thought it is worth sharing :)



## Français

BASH MIND est une version du jeux de réflexion [Mastermind](http://codebreaker.creativitygames.net/) en ligne de commande, écrite en [Bash](https://www.gnu.org/software/bash/). Vous utilisez les chiffres au lieu des billes colorées.


### Utilisation de Base

1. Copiez la version française du script : `bashmind-fr.sh` (télécharger le ou cloner le dépôt),

2. Ouvrez votre Émulateur de Terminal (évidemment),

3. Donnez la permission d'exécution au fichier

		$ chmod +x bashmind-fr.sh

4. Lancez le script et Craquez le Code !

		$ ./bashmind-fr.sh

### Personnalisation

Les paramètres du jeux, i.e. **(1) la Langueur du Code Secret**, **(2) le Nombre d'Éléments** parmi lesquels choisir et **(3) combien de Tentatives** vous sont permises, peuvent être modifier en ajoutant des options à la ligne de commande au lancement, dans l'ordre suivant :

	./bashmind-fr 4 6 8

Cette commande lance le jeux avec les paramètres ci-dessous :

1. la Longueur du Code Secret : **4** chiffres, disant "3 1 0 5",
2. la Nombre d'Éléments parmi lesquels choisir : **6** c'est les six chiffres "0 1 2 3 4 et 5",
3. Tentatives Permises : **8**.


### Prerequis 

Un Émulateur de Terminal avec Bash comme Shell.

"**Bash** est le Shell par défaut dans la majorité des distributions Linux et macOS d'Apple (ex. OS X). Récemment, une version est aussi disponible pour Windows 10." -- [Ryans Tutorials](https://ryanstutorials.net/bash-scripting-tutorial/)


### Idée

Ce script a commencé comme deux des exercices du tutoriel Bash scripting par Ryan Chadwick. Une fois le tutoriel terminé, on s'est amusé à y jouer avec des amis. J'ai pensé alors qu'il mérite peut être d'être publié :)



## Screenshots 


### English Version

![welcome screen](screenshots/04-bashmind-en-welcome-screen.png "Welcome Screen")

![pick 4 digits](screenshots/05-bashmind-en-pick-4-digits.png "Playing - Pick up 4 digits")

![round 6](screenshots/06-bashmind-en-ingame.png "Playing - 6th round")

![code found](screenshots/07-bashmind-en-bingo.png "Win Screen")


### Version Française

![ecran de bienvenu](screenshots/01-bashmind-fr-welcome-screen.png "Écran de Bienvenu")

![prenez 4 chiffres](screenshots/02-bashmind-fr-pick-4-digits.png "Début de la partie - Prenez 4 chiffres")

![partie gagnée](screenshots/03-bashmind-fr-bingo.png "Gagné")

