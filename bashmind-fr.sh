#!/bin/bash

# BASH MIND - Une version du jeux de réflexion MasterMind, écrite en GNU Bourne-Again SHell.
# Copyright (C) 2020 Awalis.

# Ce programme est un logiciel libre ; vous pouvez le redistribuer
# ou le modifier suivant les termes de la GNU General Public License
# telle que publiée par la Free Software Foundation ; soit la version 3
# de la licence, soit (à votre gré) toute version ultérieure.
# 
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; sans même la garantie tacite
# de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. 
# Consultez la GNU General Public License pour plus de détails.
# 
# Vous devez avoir reçu une copie de la GNU General Public License 
# en même temps que ce programme ; si ce n'est pas le cas,
# consultez <http://www.gnu.org/licenses>.



# Auteur :   awalis (awalis@tutanota.com)
# Version :  0.1.1
#
# MANUEL : `./bashmind.sh`
#              Démarrer le jeux avec les paramètres par défaut,
#              un écran de bienvenu explique comment jouer.
#
#          `./bashmind.sh 4 6 8`
#              Démarrer le jeux avec les paramètres suivants :
#              - Longueur du Code Secret : 4 chiffres
#              - Nombre d'éléments parmi lesquels choisir : 6
#              - Tentatives permises : 8
#
#          `chmod +x bashmind.sh`
#              Évidement, ce fichier script bash a besoin de la permission d'exécution pour démarrer.
#
# FICHIERS : Les scores gagnants sont enregistrés sur un fichiers texte
#              dans ~/{.local/share/,.local/,.}bashmind_history.txt



# LISTE DES FONCTIONS

# FONCTION :    Affiche l'ecran de bienvenu (instructions & personnalisation)
# UTILISATION : welcome_screen
welcome_screen () {
    clear -x
    echo -e ">> Appuyer une touche quelconque pour commencer.   (⌐■_■) BASHMIND   

----[ COMMENT JOUER    ]-------------------------------------------
 Bash génère un Code Secret, disant \"3 0 5 2\" et vous devez 
 le deviner/décoder. Vous pouvez essayer plusieurs fois (e.g. 8)

 Après chaque fausse tentative, deux indices vous seront donnés :
 1. Combien de chiffres correctes mis dans la bonne position,
 2. Combien de chiffres correctes mis dans une mauvaise position.
 
----[ PERSONNALISATION ]-------------------------------------------
 Vous pouvez changer les paramètres du jeux au lancement du script.

 1. Longueur du Code Secret :
      De combien de chiffres il est composé (4 par défaut);
 2. Nombre d'éléments parmi lesquels choisir : 
      Par Défaut 6, c'est les 6 chiffres \"0 1 2 3 4 et 5\";
 3. Tentatives permises : 
      À combien d'essais vous avez droit (8 par défaut).

 +------------------------+  Cette commande lancera le script avec
 |  ./bashmind.sh 5 8 10  |  un Code de 5 chiffres, le choix entre"

    # Démarrer le jeux après un appui sur n'importe quelle touche.
    # Utiliser l'invite de commande de `read` pour afficher la dernière ligne de l'écran de bienvenue

    read -n 1 -s -r -p ' +------------------------+  8 Éléments (0 à 7) avec 10 Tentatives.'
}


# FONCTION :    Afficher l'entête du jeux
# UTILISATION : display_header
display_header () {
    clear -x
    echo -e '
    ====+========+++++==================+++++========+====
                          BASH  MIND   
    ====+========+++++==================+++++========+====
'
}


# FONCTION :    Afficher les paramètres du jeux pour la session actuelle
# UTILISATION : display_game_parameters
#               - Appelée une fois que les paramètres du jeux sont règles 
display_game_parameters () {

    echo -e "
    --------------------+------------------+--------------
     Longueur du Code $SECRET_LENGTH   $ITEMS_NUMBER Éléments [0-$(($ITEMS_NUMBER - 1))]   Tentatives $TRIALS_NUMBER
    --------------------+------------------+--------------

     C'est parti !
"
}


# FONCTION :    Afficher des indices après une fausse tentative
# UTILISATION : display_hints
#               - Utiliser les variables générées par la fonction `compare_selected_with_secret_code`
display_hints () {
    echo "          + chiffres correctes dans la bonne position :     ☻ $correct_position"
    echo "          ~ chiffres correctes dans une mauvaise position : ☺ $correct_content"; echo
}


# FONCTION :    Écran à afficher si le code est a été craqué dans la limite des tentatives 
# UTILISATION : win_screen
#               - Appelée si le code saisi par le jouer est égal au code secret 
win_screen () {
    clear -x
    echo -e "
     #######################  #########  ###############
                                                       #
                  BINGO!                               #
     #       Vous venez de Craquer Le Code (⌐■_■)
     #            [ ${secret[*]} ]
                                                       #
     ## #######  ##########################  #######  ##
"
}


# FONCTION :    Écran à afficher si le code n'a pas été craqué après la dernière tentative
# UTILISATION : gameover_screen
#               - Appelée si le jouer va au-delà de la limite des tentatives sans succès   
gameover_screen () {
    clear -x
    echo -e "
     ############## ¯\_(ツ)_/¯  GAME OVER ##############

     # ## ###     Une autre fois peut-être !


**   [ CODE    ]  Le Code Secret été ................... ${secret[*]} 


**   [ ASTUCE  ]  Vous pouvez changer les paramètres du jeux :
       	          Longueur du Code, Nombre d'Eléments et Tentatives.
	          Essayez l'une des commandes suivantes :                
                  ---------------------------------------
                           ./bashmind.sh 3 5 8
                           ./bashmind.sh $SECRET_LENGTH $ITEMS_NUMBER $((TRIALS_NUMBER + 2))
                  ---------------------------------------
"
}


# FONCTION :    Affiche des statistiques à propos de la session passée du jeux
# UTILISATION : stats
stats () {
    echo -e "
**   [ STATS   ]  Durée :             $(( $SECONDS / 60 ))min. $(( $SECONDS % 60))sec.
                  Longueur du Code :  $SECRET_LENGTH
                  Nombre d'Elements : $ITEMS_NUMBER
                  Tentatives:         $counter_trials
"
}


# FONCTION :    Enregistrer les scores gagnants sur un fichier texte
# UTILISATION : log_score
log_score () {
    if [ -d $HOME/.local/share ]; then
	local file_location="$HOME/.local/share/bashmind_history.txt"
    elif [ -d $HOME/.local ]; then
	local file_location="$HOME/.local/bashmind_history.txt"
    else
	local file_location="$HOME/.bashmind_history.txt"
    fi
    echo -e "Durée ${SECONDS}s,\tTentatives ${counter_trials},\tLongueur ${SECRET_LENGTH},\tÉléments ${ITEMS_NUMBER},\t$(date -I)." >> "$file_location"
    [[ -f $file_location ]] && echo -e "\n**   [ JOURNAL ]  Score ajouté à votre fichier historique \n                  \"$file_location\""
    echo
}


# FONCTION :    Vérifier si la saisie est un nombre dans l'intervalle désirée  
# UTILISATION : input_number_valid RANGE-MIN RANGE-MAX VAR-TO-VALID
#
#           - RANGE-{MIN,MAX} peut être un numéro à deux chiffres [0-99]
#           - VAR-TO-VALID est la variable à valider, sa valeur est entre RANGE-MIN et RANGE-MAX
#           * e.g.1: `input_number_valid 3 19 5`
#           * e.g.2: `input_number_valid 6 4`
input_number_valid () {

    # Accepte 2 ou 3 paramètres uniquement, sinon SORTIE 2: nombre de paramètres erroné
    [[ ! $# == [2-3] ]] && return 2
    # Tester si les paramètres ne sont pas des nombres a 2 chiffres, SORTIE 3: paramètre(s) invalide(s) 
    for parameter in $@; do
	[[ ! $parameter =~ ^[0-9]{1,2}$ ]] && return 3
    done
    case $# in
	3)
	    # 3 paramètres fournies: RANGE-MIN RANGE-MAX VAR-TO-VALID
	    [[ $1 -ge $2 ]] && return 4 # limites invalides pour l'intervalle
	    ( [[ $3 -lt $1 ]] || [[ $3 -gt $2 ]] ) && return 1 # la saisie est hors intervalle
	    return 0 # la saisie est valide
	    ;;
	2)
	    # 2 paramètres fournies : RANGE-MAX VAR-TO-VALID, default RANGE-MIN is 0
	    [[ $2 -gt $1 ]] && return 1 # saisie hors intervalle
	    return 0 # la sasie est valide
	    ;;
    esac    
}


# FONCTION :    Régler les paramètres du jeux selon la saisie de l'utilisateur,
#               ou au valeurs par défauts en cas d'erreurs
# UTILISATION:  set_game_parameters CODE-LENGTH ITEMS-NUMBER TIALS-NUMBER
#
# SORTIE :      $SECRET_LENGTH, $ITEMS_NUMBER, TRIALS_NUMBER
set_game_parameters () {
    # Longueur du Code [3-8], par défaut 4
    if $(input_number_valid 3 8 $1); then
	SECRET_LENGTH=$1	
	# Nombre d'Éléments [SECRET_LENGTH-10], par défaut SECRET_LENGTH + 2
	if $(input_number_valid $1 10 $2); then
	    ITEMS_NUMBER=$2
	    # Nombre de Tentatives [3-99], valeur par défaut basée sur SECRET_LENGTH
	    if $(input_number_valid 3 99 $3); then
		TRIALS_NUMBER=$3
		return 0
	    else
		TRIALS_NUMBER=$(( SECRET_LENGTH * 2 - SECRET_LENGTH / 2 + 1))
		# SORTIE 3: Nombre de Tentatives [3-99]. Sélection Auto : $TRIALS_NUMBER"
		return 3
	    fi
	else
	    ITEMS_NUMBER=$(( SECRET_LENGTH + 2 ))
	    TRIALS_NUMBER=$(( SECRET_LENGTH * 2 - SECRET_LENGTH / 2 + 1 ))
	    # EXIT 2: Nombre d'Éléments [$SECRET_LENGTH-10]. Sélection Auto : $ITEMS_NUMBER
	    return 2
	fi
    else
	SECRET_LENGTH=4; ITEMS_NUMBER=6; TRIALS_NUMBER=8
	# SORTIE 1: Longueur du Code Secret [3-8]. Sélection Auto : $SECRET_LENGTH"
	return 1
    fi	
}


# FONCTION :    Générer le Code Secret, composé de différents chiffres
# UTILISATION : generate_secret SECRET-LENGTH ITEMS-NUMBER
#               - SECRET-LENGHT est la longueur du Code Secret,
#                               e.g. 7 signifie un Code Secret fait de 7 chiffres de long
#               - ITEMS-NUMBER  donne l'intervalle dans laquelle sélectioner les chiffres du Code Secret
#                               e.g. 5 fait que les chiffres du Code sont parmis les 5 chiffres "0 1 2 3 4"
#
#           * e.g. `generate_secretes 5 8`
## SORTIE :  ${secret[*]}, tableau contenant un chiffre du Code Secret
#                          dans chacune de ses cases / variables e.g. ${secret[1]} = 3
generate_secret () {
    # Pour chaque Chiffre du Code Secret
    local counter_secret=1
    while [[ $counter_secret -le $1 ]]; do
	local uniq_secret=1
	until [[ $uniq_secret == 0 ]]; do
	    # Générer un chiffre aléatoire dans l'intervalle de la sélection
	    secret[$counter_secret]=$(( $RANDOM % $2 ))
	    # Comparer le chiffre gêner avec les précédents
	    local similar_previous=0
	    local counter_avoid=$counter_secret
	    until [[ $counter_avoid -le 1 ]]; do	    
		(( counter_avoid-- ))
		# Déclarer toute similitude avec les chiffres précédents
		[[ ${secret[$counter_secret]} == ${secret[$counter_avoid]} ]] && (( similar_previous++ ))
	    done
	    # Terminer la boucle quand le chiffre généré est unique
	    [[ $similar_previous == 0 ]] && uniq_secret=0
	done
	(( counter_secret++ ))
    done
}


# FONCTION :    Valider les elements du Code saisies par le joueur
# UTILISATION : selected_code_validation
#               - À utiliser après la lecture (read) du Code saisi par le joueur
selected_code_validation () {
    # Vérifier le nombre des éléments saisis
    local counter_selected=1
    while [ $counter_selected -le $SECRET_LENGTH ]; do
	# Si des des elements manquent, SORTIE 2: Éléments manquants
	[[ -z ${selected[$counter_selected]} ]] && return 2
	(( counter_selected++ ))
    done
        # S'il y a des éléments en plus, SORTIE 3 : élément(s) en excès 
    [[ -n ${selected[$(( counter_selected++ ))]} ]] && return 3
    # Vérifier que les éléments saisies sont des nombres dans l'intervalle désirée 
    local counter_selected=1
    while [[ $counter_selected -le $SECRET_LENGTH ]]; do
	# Verifier si l'élément saisie est hors intervalle, SORTIE 1 : élément(s) hors intervalle
        [[ ${selected[$counter_selected]} != [0-$(( ITEMS_NUMBER - 1))] ]] && return 1
	(( counter_selected++ ))
    done
    # Vérifier la répétition de valeurs saisies 
    local counter_select=1
    while [ $counter_select -le $SECRET_LENGTH ]; do
	local counter_compare=$(( $counter_select + 1 ))
	while [ $counter_compare -le $SECRET_LENGTH ]; do
	    # En cas de répétition dans la saisie, SORTIE 4: Élément répété 
	    [[ ${selected[$counter_select]} == ${selected[$counter_compare]} ]] && return 4
	    (( counter_compare++ ))
	done
	(( counter_select++ ))
    done
    # Si les Élément sont dans l'intervalle désirée, SORTIE 0 : la saisie du Code est valide
    return 0
}


# FONCTION :    Comparer le code saisi par le joueur avec le Code Secret
# UTILISATION : compare_selected_with_secret_code
#               - Appelée après l'obtention d'une saisie valide du code par l'utilisateur,
#                 cette fonction compare chaque variable des deux tableaux ${selected[*]}
#                 et ${secret[*]} respectivement pour trouver si les deux codes sont égaux
# SORTIE :      $correct_position, $correct_content
compare_selected_with_secret_code () {
    
    correct_position=0
    correct_content=0
    # Pour chaque chiffre saisi
    local counter_selected=1
    while [[ $counter_selected -le $SECRET_LENGTH ]]; do
	# Pour chaque chiffre secret
	local counter_secret=1
	while [[ $counter_secret -le $SECRET_LENGTH ]]; do
	    # Si le chiffre sélectionné et le secret sont a des positions différentes
	    if [[ $counter_selected -ne $counter_secret ]]; then
		[[ ${selected[$counter_selected]} -eq ${secret[$counter_secret]} ]] && (( correct_content++ ))
	    else
		# Si le chiffre sélectionné et le secret sont à la même position
		[[ ${selected[$counter_selected]} -eq ${secret[$counter_secret]} ]] && (( correct_position++ ))
	    fi
	    (( counter_secret++ ))
	done
	(( counter_selected++ ))
    done
}

# FIN DE LA LISTE DES FONCTIONS



# CORPS DU SCRIPT

case $# in
    0)
	# Si aucun paramètre n'est fourni au lancement du script,
	# alors affiche l'écran de bienvenu et utilise les valeurs par défaut
	welcome_screen
	SECRET_LENGTH=4; ITEMS_NUMBER=6; TRIALS_NUMBER=8
	;;
    *)
	# Sinon, vérifie les paramètres fournis, et règle les valeurs en accord
	else, verify the issued game parameters, and set them accordingly
	set_game_parameters $1 $2 $3
	input_parameter_issue=$?
	;;
esac

generate_secret $SECRET_LENGTH $ITEMS_NUMBER
display_header
# Informer le joueur que les valeurs par défaut ont été utilisées à cause d'une faute au lancement
case $input_parameter_issue in
    1)
	echo "**   [ PARAM. ERRONÉ ] Longueur du Code Secret [3-8]. Sélection Auto : $SECRET_LENGTH"
    ;;
    2)
	echo "**   [ PARAM. ERRONÉ ] Nombre d'Éléments [$SECRET_LENGTH-10]. Sélection Auto : $ITEMS_NUMBER"
    ;;
    3)
	echo "**   [ PARAM. ERRONÉ ] Tentatives [3-99]. Sélection Auto : $TRIALS_NUMBER"
    ;;
esac

display_game_parameters

# Jouer le jeux dans la limite des tentatives permises
counter_trials=1
while [[ $counter_trials -le $TRIALS_NUMBER ]]; do
    [[ $counter_trials -gt 1 ]] && display_hints
    
    echo -n "     #${counter_trials} - "
    # Demander au joueur de choisir les chiffres du code, première fois 
    read -p "Prenez $SECRET_LENGTH chiffres de [0-$(( ITEMS_NUMBER - 1 ))], séparés par des espaces : " selected[1] selected[2] selected[3] selected[4] selected[5] selected[6] selected[7] selected[8]; echo
    # Répéter la demande en cas de faute de saisie 
    until $(selected_code_validation); do
	# Informer l'utilisateur des fautes de saisie précédentes 
	case $? in
	    1)
		echo "**   [ Choix Invalide ] Élément(s) Hors Limites"; echo
		;;
	    2)
		echo "**   [ Choix Invalide ] Élément(s) Manquant(s)"; echo
		;;
	    3)
		echo "**   [ Choix Invalide ] Élément(s) en Excès"; echo
		;;
	    4)
		echo "**   [ Choix Invalide ] Élément(s) Répété(s)"; echo
	esac	
	# Lire les chiffres du code saisi
	read -p "     Essayez encore $SECRET_LENGTH chiffres de [0-$(( ITEMS_NUMBER - 1 ))], séparés par des espaces : " selected[1] selected[2] selected[3] selected[4] selected[5] selected[6] selected[7] selected[8]; echo
    done

    # Comparer le code saisi avec le Code Secret
    compare_selected_with_secret_code
    # Resultat de la comparaison 
    if [[ $correct_position -eq $SECRET_LENGTH ]]; then
	win_screen
	log_score
	break 
    fi
    (( counter_trials++ ))
done

if [[ $counter_trials -gt $TRIALS_NUMBER ]]; then
    gameover_screen
    (( counter_trials-- ))
fi

# Afficher des statistique sur la partie
stats

# Enlever les variables crées 
unset SECRET_LENGTH ITEMS_NUMBER TRIALS_NUMBER counter_trials secret[*] selected[*] correct_position correct_content
[[ -v input_parameter_issue ]] && unset input_parameter_issue

